"""Script to reset time on CardIO via serial, run on HOST computer

inspired by: https://badge.team/projects/suckless_flash
"""

from __future__ import print_function
import sys, time

dev=open("/dev/ttyACM0", "w")
print(chr(0x03), end="", file=dev)
cmd = "import utime; utime.set_unix_time({})\r"
print(cmd.format(int(time.time())), file=dev)
print(chr(0x04), end="", file=dev)
dev.close()
